module PySupervisor where

import Prelude

import Data.Maybe (Maybe(..))
import Data.Time.Duration (Milliseconds(..), Seconds(..))
import Effect (Effect)
import PyGenServer as PyGenServer
import Erl.Atom (atom)
import Erl.Data.List (nil, (:))
import Erl.Process.Raw (class HasPid)
import Pinto (RegistryName(..), StartLinkResult)
import Pinto.Supervisor (ChildShutdownTimeoutStrategy(..), ChildType(..), ErlChildSpec, RestartStrategy(..), Strategy(..), SupervisorPid, SupervisorSpec, spec)
import Pinto.Supervisor as Sup

startLink :: Effect (StartLinkResult SupervisorPid)
startLink = do
  Sup.startLink (Just $ Local $ atom "py_supervisor") init

init :: Effect SupervisorSpec
init = do
  pure
    { flags:
        { strategy: OneForOne
        , intensity: 1
        , period: Seconds 5.0
        }
    , childSpecs:
        (worker "py_gen_server" $ PyGenServer.startLink {})
        : nil
    }

-- A convenience function to set up a child with defaults
-- Note: There is no rule here saying that a child process should be a GenServer or a GenStateM, it just needs 
-- to be something that has a pid (and for erlang itself, should probably use proc_lib..)
worker ::
  forall childProcess.
  HasPid childProcess =>
  String -> Effect (StartLinkResult childProcess) -> ErlChildSpec
worker id start =
  spec
    { id
    , childType: Worker
    , start
    , restartStrategy: RestartTransient
    , shutdownStrategy: ShutdownTimeout $ Milliseconds 5000.0
    }