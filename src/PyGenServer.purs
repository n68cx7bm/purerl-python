module PyGenServer where

import Prelude

import Effect (Effect)
import Effect.Class (liftEffect)
import Effect.Console (log)


import Erl.Atom (Atom, atom, toString)
import Erl.Process.Raw (Pid)
import Erl.Data.List.Types (List)
import Data.Maybe (Maybe(..))
import Pinto (RegistryName(..), StartLinkResult)
import Pinto.Types
import Pinto.GenServer (InitResult(..), ServerPid, ServerType)
import Pinto.GenServer as GenServer

import PyTypes

type EmptyGenServerStartArgs
  = {}

type State
  = {python :: Pid}

serverName :: RegistryName (ServerType Unit Unit Unit State)
serverName = Local $ atom "py_gen_server"

startLink :: EmptyGenServerStartArgs -> Effect (StartLinkResult (ServerPid Unit Unit Unit State))
startLink args = GenServer.startLink $ (GenServer.defaultSpec $ init args) { name = Just serverName }

init :: EmptyGenServerStartArgs -> GenServer.InitFn Unit Unit Unit State
init _args = do
  pid <- liftEffect $ startPython
  pure $ InitOk { python: pid }

callpy :: Atom -> Atom -> List PyType -> Effect String
callpy mod funct args = GenServer.call (ByName serverName) (\_from state -> do
  v <- liftEffect $ pyInt state.python mod funct args
  pure $ GenServer.reply (show v) state)
