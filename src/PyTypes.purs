module PyTypes where

import Prelude

import Effect (Effect)

import Erl.Data.Tuple
import Erl.Data.List.Types (List)
import Erl.Atom (Atom)
import Erl.Process.Raw (Pid)


-- Basic erlport python functionality
foreign import getVersion :: Effect String
foreign import startPython :: Effect Pid
foreign import callPython :: Pid -> Atom -> Atom -> List PyType -> Effect PyType
foreign import killPython :: Pid -> Effect Atom

-- Convenience functions for functions with known return type
foreign import pyInt :: Pid -> Atom -> Atom -> List PyType -> Effect Int
foreign import pyStr :: Pid -> Atom -> Atom -> List PyType -> Effect String
foreign import pyNum :: Pid -> Atom -> Atom -> List PyType -> Effect Number
foreign import pyBool :: Pid -> Atom -> Atom -> List PyType -> Effect Boolean


-- Python type definition
data PyType = PyType | PyInt Int 
  | PyNum Number | PyBool Boolean 
  | PyStr String | PyAtom Atom 
  | Tuple1 PyType | Tuple2 PyType PyType | Tuple3 PyType PyType PyType
  | List PyType


instance showPyType :: Show PyType where
  show (List a) = show a
  show (Tuple3 a b c) = "(" <> show a <> ", " <> show b <> ", " <> show c <> ")"
  show (Tuple2 a b ) = "(" <> show a <> ", " <> show b <> ")"
  show (Tuple1 a ) = "(" <> show a <> ")"
  show (PyInt a) = show a
  show (PyNum a) = show a
  show (PyBool a) = show a
  show (PyStr a) = show a
  show (PyAtom a) = show a
  show PyType = "<<opaque pytype>>"

