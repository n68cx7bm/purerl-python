module PyApp where

import PySupervisor as PySupervisor
import Pinto.App as App

-- Our entry point is not tremendously exciting
-- but it is an entry point
start = App.simpleStart PySupervisor.startLink