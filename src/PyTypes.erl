
-module(pyTypes@foreign).

-export([getVersion/0, startPython/0, callPython/4, killPython/1]).
-export([pyInt/4,pyNum/4,pyStr/4,pyBool/4]).

getVersion() ->
	fun() ->
		{ok, P} = python:start(),
		Resp = python:call(P, sys, 'version.__str__', []),
		python:stop(P),
		Resp
	end.

startPython() ->
	fun() ->
		{ok, P} = python:start(),
		P
	end.

callPython(P, Module, Funct, ArgsList) ->
	fun() ->
		Resp = python:call(P, Module, Funct, ArgsList),
		Resp
	end.

killPython(P) ->
	fun() ->
		Resp = python:stop(P),
		Resp
	end.

pyInt(P, Module, Funct, ArgsList)-> callPython(P, Module, Funct, ArgsList).
pyNum(P, Module, Funct, ArgsList)-> callPython(P, Module, Funct, ArgsList).
pyStr(P, Module, Funct, ArgsList)-> callPython(P, Module, Funct, ArgsList).
pyBool(P, Module, Funct, ArgsList)-> callPython(P, Module, Funct, ArgsList).