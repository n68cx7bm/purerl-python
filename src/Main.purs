module Main where

import Prelude

import Effect (Effect)
import Effect.Console (log)


foreign import printSomething :: Effect Unit

main :: Effect Unit
main = do
  log "works!!"
  printSomething
